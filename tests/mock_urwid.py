#!/usr/bin/env python3

import re
import sys
#import importlib
import runpy
from collections.abc import Callable, Iterator

import urwid
import urwid.raw_display
import urwid.curses_display

import pytest


@pytest.fixture
def urwid_mock(monkeypatch: pytest.MonkeyPatch) -> 'Iterator[UrwidMock]':
	monkeypatch.setattr(urwid, 'MainLoop', MockLoop)
	monkeypatch.setattr(urwid.raw_display, 'Screen', MockScreen)
	monkeypatch.setattr(urwid.curses_display, 'Screen', MockScreen)
	yield UrwidMock()


class UrwidMock:

	# ------- init -------

	def __init__(self) -> None:
		MockLoop.urwid_mock = self
		self.mockloop: 'MockLoop|None' = None
		self._mockloop_run: 'Callable[[], None]|None' = None

	def set_mock_loop_object(self, mockloop: 'MockLoop') -> None:
		assert self._mockloop_run, "urwid_mock.set_mock_loop has not been called"
		mockloop.run = self._mockloop_run  # type: ignore [method-assign]
		self.mockloop = mockloop

	# ------- setup -------

	def set_size(self, x: int, y: int) -> None:
		MockLoop.size = (x, y)
		MockScreen.size = (x, y)

	def set_mock_loop(self, func: 'Callable[[], None]') -> None:
		self._mockloop_run = func

	# ------- loop -------

	def get_lines(self) -> 'list[bytes]':
		assert self.mockloop, "MockLoop has not been initialized"
		return self.mockloop.get_lines()

	def simulate_key_press(self, key: str) -> None:
		assert self.mockloop, "MockLoop has not been initialized"
		self.mockloop.simulate_key_press(key)

class MockLoop:

	size: 'tuple[int, int]'
	urwid_mock: 'UrwidMock'

	def __init__(self,
		widget: urwid.Widget,
		palette: object, *,
		input_filter: 'Callable[[list[str], list[int]], list[str]]|None' = None,
		unhandled_input: 'Callable[[str], bool]',
		handle_mouse: bool,
		screen: 'urwid.Screen',
	) -> None:
		if input_filter is None:
			input_filter = lambda keys, raws: keys
		self.widget = widget
		self.palette = palette
		self.input_filter = input_filter
		self.unhandled_input = unhandled_input
		self.urwid_mock.set_mock_loop_object(self)

	def run(self) -> None:
		raise NotImplementedError()

	def get_lines(self) -> 'list[bytes]':
		return self.widget.render(self.size).text

	def simulate_key_press(self, key: str) -> bool:
		'''
		The urwid documentation says "The unhandled_input function should return True if it handled the input." [description of MainLoop.unhandled_input]
		But the return value is not checked and none of the official examples returns something from unhandled_input.
		Therefore I am not returning anything from the unhandled_input function either, making the return value of this method mean:
		True if it has been handled by widget, None if it has been passed to unhandled_input.
		'''
		keys = self.input_filter([key], [-1])
		assert len(keys) == 1
		key = keys[0]
		k = self.widget.keypress(self.size, key)
		if k:
			return self.unhandled_input(key)
		return True

class MockScreen:

	size: 'tuple[int, int]'

	def start(self) -> None:
		pass

	def get_cols_rows(self) -> 'tuple[int, int]':
		return self.size

	def register_palette_entry(*palette_tuple: 'object') -> None:
		pass


class Line:

	def __init__(self, pattern: bytes) -> None:
		self.reo = re.compile(pattern)

	def __eq__(self, other: object) -> bool:
		if isinstance(other, bytes):
			return bool(self.reo.match(other))
		return NotImplemented

	def __repr__(self) -> str:
		return '%s(%r)' % (type(self).__name__, self.reo.pattern)



def run(*cmd: str) -> None:
	original_argv = sys.argv
	try:
		sys.argv = list(cmd)
		app = cmd[0]
		if app == 'gitl':
			name = 'log'
		elif app == 'gitd':
			name = 'diff'
		elif app == 'gits':
			name = 'show'
		else:
			raise ValueError("unknown program %r" % app)
		#spec = importlib.util.find_spec('git_viewer.%s' % name)
		#assert spec is not None
		#assert spec.loader is not None
		#module = importlib.util.module_from_spec(spec)
		#spec.loader.exec_module(module)
		runpy.run_module('git_viewer.%s' % name, run_name='__main__')
	finally:
		sys.argv = original_argv
