#!/usr/bin/env bash

# ------- strict mode -------

set -euo pipefail
IFS="$(printf '\n\t')"


# ------- clean up last build -------

if [ -e dist ]; then
	rm -r dist
fi
if [ -e src/git_viewer.egg-info ]; then
	rm -r src/git_viewer.egg-info
fi


# ---------- functions ----------

color_reset='[m'
color_cmd='[34m'
color_error='[31m'

run() {
	# $ run echo "hello world"
	# works for commands with arguments
	# does not work for pipes, redirects and stuff
	echo "$color_cmd\$ $@$color_reset"
	"$@"
}

echo_error() {
	echo "${color_error}ERROR: $@$color_reset"
}


# ------- venv -------

if [ -d venv ]; then
	. venv/bin/activate
else
	echo_error 'venv does not exist, create it with ./release --update-venv'
fi


# ------- build -------

run python3 -m build


# ------- test that build contains important files -------

run python3 -m twine check dist/*

ends_with() { [ "${1%$2}" != "$1" ]; }
assert_exists() {
	if ! [ -e "$1" ]; then
		echo
		echo_error "${1#$outd/} is missing in $fn"
		exit 1
	fi
}

for fn in dist/*; do
	outd="$(mktemp -d)"
	if ends_with "$fn" ".tar.gz"; then
		run tar -xz -f "$fn" -C "$outd"

		assert_exists "$outd"/git_viewer-*/PYPI_README.md
		assert_exists "$outd"/git_viewer-*/src/git_viewer/doc/version.txt

	elif ends_with "$fn" ".whl"; then
		run unzip "$fn" -d "$outd"

		assert_exists "$outd/git_viewer/doc/version.txt"

	else
		echo "unexpected file: $fn"
		exit 1
	fi
	rm -r "$outd"
done
